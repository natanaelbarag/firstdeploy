FROM openjdk:14

ADD target/First-deployed-project-0.0.1-SNAPSHOT.jar app/
WORKDIR app/
EXPOSE 8989
ENTRYPOINT ["java",  "-jar" , "First-deployed-project-0.0.1-SNAPSHOT.jar"]