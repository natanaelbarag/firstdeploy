package com.first.First.deployed.project;

import com.first.First.deployed.project.rest.Student;
import com.first.First.deployed.project.rest.StudentController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class FirstDeployedProjectApplicationTests {

	@Autowired
	private StudentController studentController;

	@Test
	public void getStudentById1ShouldReturnJamesBond(){
		Student studentById = studentController.getStudentById(1);
		assertThat(studentById.getStudentId()).isEqualTo(1);
		assertThat(studentById.getStudentName()).isEqualTo("James Bond");
	}

}
