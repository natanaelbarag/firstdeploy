package com.first.First.deployed.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstDeployedProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstDeployedProjectApplication.class, args);
	}

}
