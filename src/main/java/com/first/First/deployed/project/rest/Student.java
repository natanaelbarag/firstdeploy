package com.first.First.deployed.project.rest;

public class Student {
    private final String studentName;
    private final int studentId;

    public Student(int studentId, String studentName) {
        this.studentId = studentId;
        this.studentName = studentName;
    }

    public String getStudentName() {
        return studentName;
    }

    public int getStudentId() {
        return studentId;
    }
}
